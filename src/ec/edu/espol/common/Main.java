/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;
import java.util.Scanner;
import ec.edu.espol.util.*;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.mail.MessagingException;
/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws javax.mail.MessagingException
     * @throws java.security.NoSuchAlgorithmException
     */
    public static void main(String[] args) throws IOException, MessagingException, NoSuchAlgorithmException {
        // TODO code application logic here
        System.out.println("\t\t\t\t\t\t\t\t\tCounting Cars\n\t\t\t\t\t\t\t\tCompra - Venta de Vehículos\n\nEn esta aplicación podrá COMPRAR el vehículo que más le guste haciendo ofertas convenientes, o VENDER su vehículo al precio que le guste.");
        
        int respuesta = 0,vehiculo = 0;
        boolean error=false;
        String asunto, cuerpo;
        Sistema.creacionArchivos();
        System.out.println("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
        do{
            System.out.print("Menú de opciones\n¿Qué desea hacer?\n1. Vender vehículo\n2. Comprar vehículo\n3. Salir\nSu respuesta » ");
            do{
                try{
                    Scanner sc = new Scanner(System.in);
                    respuesta = sc.nextInt();
                    error=false;
                    if(respuesta<1||respuesta>3){
                        System.out.println("\n*********************************************************************************************\n[#]No ingreso ningúna de las opciones mencionadas.");
                        error = true;
                }}catch(Exception e){
                    error = true;
                    System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número");
                }
            }while((respuesta<1||respuesta>3)&&(error));
            Persona user;
            Rol rol;
            Oferta offer = null;
            Vehiculo veh;
            Moto m;
            Venta venta;
            String us, email, pw;
            switch (respuesta) {
                case 1:
                {
                    int regresar;
                    do{
                        System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                        int vendedor = 0;
                        do{
                            try{
                                vendedor=Sistema.numeroPorTeclado("Opciones del vendedor.\n1. Registrar un nuevo vendedor\n2. Ingresar un nuevo vehículo\n3. Aceptar oferta\n4. Regresar\nSu respuesta » ");
                                error = false;
                                if(vendedor<1||vendedor>4){
                                    System.out.println("\n*********************************************************************************************\n[#]No ingreso ningúna de las opciones mencionadas.");
                                    error = true;
                                }}catch(Exception e){
                                    error = true;
                                    System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número");
                        }}while((vendedor<1||vendedor>4)&&(error));
                        switch (vendedor) {
                            case 1:
                            {
                                System.out.println("\n------------------------------------------------------------------------------------------\n");
                                System.out.println("->Registro\nNota: favor de ingresar correctamente datos únicos suyos y asegurarse que tiene cuenta de gmail para recibir información del programa,\nSaludos.");
                                us = Sistema.textoPorTeclado("\nUsuario: ");
                                email = Sistema.validarGmail();
                                if(Sistema.existeUsuario(us,email,"vendedor")==2||Sistema.numeroLineas("Usuarios.txt")==0){
                                    pw = Sistema.textoPorTeclado("Clave: ");
                                    user=Sistema.infoPersona1();
                                    asunto = "Creación de cuenta";
                                    cuerpo = "Se ha creado correctamente la cuenta.\nRecuerde que su usuario es: \""+us+"\" y contraseña es: \""+pw+"\".\nConsiderar que la misma contraseña";
                                    Sistema.enviarConGMail(email, asunto, cuerpo);
                                    
                                    rol = new Rol(us, pw, email,"vendedor");
                                    user.añadirCuenta(rol);
                                    rol.guardarClave();
                                }
                                else if(Sistema.existeUsuario(us,email,"vendedor")==3)
                                    Rol.modificarCuenta(us, "comprador", "vendedor");
                                else
                                    System.out.println("Ya existe un usuario así como vendedor.\nPuede intentar con otro si gusta.");
                            }break;
                            case 2:
                            {
                                int intentos = 0;
                                System.out.println("Ingrese los datos de su cuenta venderdor:");
                                do{
                                    us = Sistema.textoPorTeclado("Usuario: ");
                                    pw=Sistema.textoPorTeclado("Clave: ");
                                    if(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("vendedor", us)))&&(intentos<3)){
                                        System.out.println("\n*********************************************************************************************\n[X] El usuario y/o contraseña no son los correctos. Intente nuevamente");
                                        ++intentos;
                                    }
                                }while(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("vendedor", us)))&&(intentos<3));
                                if(intentos == 3){
                                    System.out.println("\n*********************************************************************************************\n[¿?] Al parecer no existe esa cuenta o archivo, vuelva a intentarlo o cree una cuenta.");
                                }
                                else{
                                    String placa = "",motor = "", transmision = "";
                                    int ruedas, pasajeros, espejo;
                                    double precio = 0;
                                    do{
                                        System.out.println("------------------------------------------------------------------------------------------\nBienvenido, "+us+"\nSeleccione el tipo de vehículo a vender.\n1. Carro\t2. Moto\t\t3. Camioneta\n4. Camion\t5. Bus\t\t6.Furgoneta");
                                        try{
                                            Scanner sc = new Scanner(System.in);
                                            vehiculo = sc.nextInt();
                                            if(vehiculo<1||vehiculo>6){
                                                System.out.println("\n*********************************************************************************************\n[#]No ingreso ningúna de las opciones mencionadas.");
                                                error=false;
                                        }}catch(Exception e){
                                            error = true;
                                            System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número");
                                        }
                                    }while((vehiculo<1||vehiculo>6)&&(error));
                                        System.out.println("Ingrese los datos de su vehículo, los datos que contengan un asterísco [*] son prioridad.\n[Nota]:  En caso de no saber una característica, es recomendable que escriba NA");
                                        do{
                                            placa = Sistema.textoPorTeclado("Placa*: ");
                                        }while(Sistema.verificarPlaca(placa));
                                        String marca = Sistema.VerificarString("Marca: ");
                                        
                                        int año = Sistema.numeroPorTeclado("Año en que el vehículo fué comprado: ");
                                        String color=Sistema.VerificarString("Color: ");
                                        do{
                                            motor = Sistema.textoPorTeclado("Tipo de motor\nElija \"E\" para eléctrico, \"C\" para combustión o \"H\" para híbrido: ");
                                            if(!motor.equals("E")&&!motor.equals("C")&&!motor.equals("H")){
                                                System.out.println("Ingrese únicamente \"E\" para un motor eléctrico, \"C\" para uno de combustión o \"H\" para uno híbrido.");
                                            }
                                        }while(!motor.equals("E")&&!motor.equals("C")&&!motor.equals("H"));
                                        
                                        if(motor.equals("H"))
                                            motor = "Hibrido";
                                        else if(motor.equals("E"))
                                            motor = "Electrico";
                                        else if(motor.equals("C"))
                                            motor = "Combustion";
                                        
                                        int recorrido = Sistema.numeroPorTeclado("Recorrido*: ");
                                        
                                        String tipo_frenos = Sistema.VerificarString("Tipo de frenado: ");
                                        if(motor.equals("C")||motor.equals("H")){
                                            do{
                                                transmision= Sistema.textoPorTeclado("Estilo de arranque\nEscriba mecánico o automático: ");
                                                if(!transmision.equals("mecanico")&&!transmision.equals("automatico")){
                                                    System.out.println("Ingrese únicamente mecanico y automatico.");
                                                }
                                            }while(!transmision.equals("mecanico")&&!transmision.equals("automatico"));
                                        }
                                        else
                                            transmision = "automatico";
                                        String modelo = Sistema.textoPorTeclado("Modelo: ");

                                    switch(vehiculo){
                                        case 1:
                                        {
                                            
                                            
                                           ruedas=4;
                                             
                                             
                                              System.out.println("Ingrese valores permitidos");
                                             do{
                                                 espejo = Sistema.numeroPorTeclado("Inserte el número de espejos del vehículo: ");
                                              
                                             }while(espejo<=0);
                                             
                                            do{ 
                                        
                                            pasajeros = Sistema.numeroLineas("Inserte la capacidad promedia de pasajeros: ");
                                            } while( pasajeros<0);
                                            
                                              System.out.println("No existen pasajeros negativos");
                                            
                                            boolean descapotable;
                                            String resp;
                                            int rep=0;
                                            do{
                                             resp = Sistema.textoPorTeclado("¿EL carro es descapotable?(Y/N): ");
                                             if(resp.equals("Y")){
                                                 rep++;
                                                descapotable = true;}
                                             else if(resp.equals("N")){
                                                 descapotable=false;
                                                 rep++;
                                             }
                                             else{
                                                descapotable=false;}
                                           
                                             
                                            }while(rep==0);
                                            
                                            
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            venta = new Venta(precio);
                                            Carro car = new Carro(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo,descapotable);
                                            car.agregarVehiculo( us, veh, venta,"Carro");
                                            
                                            
                                        }break;
                                        
                                        case 2:
                                        {
                                            espejo = 2;
                                            pasajeros = 2;
                                            ruedas=2;
                                            String tipo_timon = "";
                                            try{
                                                tipo_timon = Sistema.textoPorTeclado("Tipo de timon: ");
                                               
                                            }catch(Exception e){}
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            venta = new Venta(precio);
                                            Moto moto = new Moto(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo, tipo_timon);
                                            moto.agregarVehiculo( us, veh, venta,"Moto");
                                        }break;
                                        case 3:
                                        {
                                           ruedas=4;
                                             System.out.println("Ingrese valores permitidos");
                                             do{
                                                 espejo = Sistema.numeroPorTeclado("Inserte el número de espejos del vehículo: ");
                                              
                                             }while(espejo<=0);
                                              
                                             
                                            do{ 
                                        
                                            pasajeros = Sistema.numeroLineas("Inserte la capacidad promedia de pasajeros: ");
                                            } while( pasajeros<0);
                                              System.out.println("No existen pasajeros negativos");
                                            
                                            boolean descapotable;
                                            String resp;
                                            
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            venta = new Venta(precio);
                                            Camioneta cm= new Camioneta(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo,espejo+"");
                                            cm.agregarVehiculo(us, veh, venta, "Camioneta");
                                            
                                            
                                        }break;
                                        case 4:
                                        {
                                            
                                            ruedas=4;
                                             
                                             
                                              System.out.println("Ingrese valores permitidos");
                                             do{
                                                 espejo = Sistema.numeroPorTeclado("Inserte el número de espejos del vehículo: ");
                                              
                                             }while(espejo<=0);
                                            do{ 
                                        
                                            pasajeros = Sistema.numeroLineas("Inserte la capacidad promedia de pasajeros: ");
                                            } while( pasajeros<0);
                                              System.out.println("No existen pasajeros negativos");
                                            
                                            boolean camarote;
                                            String resp;
                                            
                                            
                                            double carga = Sistema.decimalPorTeclado("Ingrese la carga: ");
                                            String resp1;
                                            int rep=0;
                                            do{
                                             resp1 = Sistema.textoPorTeclado("¿EL camion tiene camarote?(Y/N): ");
                                             if(resp1.equals("Y")){
                                                 rep++;
                                                camarote = true;}
                                             else if(resp1.equals("N")){
                                                 camarote=false;
                                                 rep++;
                                             }
                                             
                                             else{
                                                camarote=false;}
                                           
                                             
                                            }while(rep==0);
                                            
                                            int ejes= Sistema.numeroPorTeclado("ingrese el numero de ejes: ");
                                            
                                            ruedas=4;
                                            
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            venta = new Venta(precio);
                                            Camion cmo= new Camion(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo,carga,camarote,ejes);
                                            cmo.agregarVehiculo(us, veh, venta, "Camion");
                                        }break;
                                        case 5:
                                        {
                                            ruedas=4;
                                             
                                             
                                              System.out.println("Ingrese valores permitidos");
                                              System.out.println("BUssss");
                                             do{
                                                 System.out.println("En bucle.........");
                                                 espejo = Sistema.numeroPorTeclado("Inserte el número de espejos del vehículo: ");
                                              
                                             }while(espejo<=0);
                                            
                                           
                                              
                                              
                                            
                                            pasajeros = Sistema.numeroLineas("Inserte la capacidad promedia de pasajeros: ");
                                            int vidrios=Sistema.numeroPorTeclado("Ingrese la cantidad de vidrios: ");
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            venta = new Venta(precio);
                                            ruedas=4;
                                            venta = new Venta(precio);
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            Bus bus= new Bus(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo,vidrios);
                                            bus.agregarVehiculo(us, veh, venta, "Bus");
                                            
                                        }break;
                                        case 6:
                                        {
                                            ruedas=4;
                                             
                                             
                                              System.out.println("Ingrese valores permitidos");
                                            
                                            do{
                                                 espejo = Sistema.numeroLineas("Inserte el número de espejos del vehículo: ");
                                              
                                             }while(espejo<=0);
                                            pasajeros = Sistema.numeroLineas("Inserte la capacidad promedia de pasajeros: ");
                                            int asientos=Sistema.numeroPorTeclado("Ingrese el numero de asientos: ");
                                            do{
                                                try{
                                                    precio = Sistema.decimalPorTeclado("Precio del vehículo*: ");
                                                    error = false;
                                                }catch(Exception e){
                                                    error=true;
                                                }
                                            }while(error);
                                            venta = new Venta(precio);
                                            ruedas=4;
                                           venta = new Venta(precio);
                                            veh = new Vehiculo(placa, marca, color, motor, ruedas, año);
                                            Furgoneta fr= new Furgoneta(recorrido, pasajeros, espejo, tipo_frenos, transmision, modelo,asientos);
                                            fr.agregarVehiculo(us, veh, venta, "Furgoneta");
                                        }break;
                                    }
                                }
                            }
                                break;
                            case 3:
                            {
                                int intentos = 0;
                                ArrayList<String> ofertas = Sistema.leerLineasArchivos("Ofertas.txt");
                                System.out.println("Ingrese los datos de su cuenta vendedor:");
                                do{
                                    us = Sistema.textoPorTeclado("Usuario: ");
                                    pw=Sistema.textoPorTeclado("Clave: ");
                                    if(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("vendedor", us)))&&(intentos<3)){
                                        System.out.println("\n*********************************************************************************************\n[X] El usuario y/o contraseña no son los correctos. Intente nuevamente");
                                        ++intentos;
                                    }
                                }while(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("vendedor", us)))&&(intentos<3));
                                if(intentos == 3){
                                    System.out.println("\n*********************************************************************************************\n[¿?] Al parecer no existe esa cuenta, vuelva a intentarlo o cree una cuenta.");
                                }
                                else{
                                    String placa;
                                    int intentox=0;
                                    do{
                                        placa = Sistema.textoPorTeclado("Ingrese la placa del vehículo el cuál quiere verificar sus ofertas: ");
                                        intentox++;
                                    }while(!Sistema.verificarPlaca(placa));
                                    if(intentox==3){
                                        System.out.println("No existen ofertas con esa placa");
                                    }                                    
                                    else{
                                    int contador = 1;
                                    for(String offex : ofertas){
                                        String plac = offex.split(">")[0];
                                        String[] arr_ofertas = offex.split(">")[1].split(";");
                                        String tema = "¡Enhorabuena! ferta de Vehículo aceptada";
                                        String desarrollo = "El usuario "+us+" Trata de ponerse en contacto con usted ya que aceptó su oferta.\nAgradecemos su gentileza por utilizar nuestra aplicación, esperamos que le sirva para otra ocasión.\nSaludos,\nCounting Cars";       
                                        boolean salida = true;
                                        if(plac.equals(placa)){
                                            for(int i=0; i<arr_ofertas.length;i++){
                                                if(i==0 && salida){
                                                    System.out.println("Oferta N°"+contador+"\nCorreo del comprador: "+arr_ofertas[i].split("-")[0]+"\nPrecio de Oferta: "+arr_ofertas[i].split("-")[1]);
                                                    contador++;
                                                    int decision = 0;
                                                    do{
                                                        decision =Sistema.numeroPorTeclado("\n1. Siguiente Oferta\t\t2. Aceptar Oferta\t\t3.SALIR\nSu respuesta » ");
                                                    }while(decision<1||decision>3);
                                                    switch(decision){
                                                        case 2:
                                                        {
                                                            Venta.aceptarOferta(arr_ofertas[i].split("-")[0], tema, desarrollo, placa);
                                                            salida=false;
                                                        }break;
                                                        case 3:
                                                        {
                                                            salida=false;
                                                        }
                                                    }
                                                }
                                                else if (i==arr_ofertas.length&&salida){
                                                    System.out.println("Oferta N°"+contador+"\nCorreo del comprador: "+arr_ofertas[i].split("-")[0]+"\nPrecio de Oferta: "+arr_ofertas[i].split("-")[1]);
                                                    contador++;
                                                    int decision = 0;
                                                    do{
                                                        decision =Sistema.numeroPorTeclado("\n1. Anterior Oferta\t\t2. Aceptar Oferta\t\t3.Salir\t\t\nSu respuesta » ");
                                                    }while(decision<1||decision>3);
                                                    switch(decision){
                                                        case 1:
                                                        {
                                                            i-=2;
                                                        }break;
                                                        case 2:
                                                        {
                                                            Venta.aceptarOferta(arr_ofertas[i].split("-")[0], tema, desarrollo, placa);
                                                            salida=false;
                                                        }
                                                        case 3:
                                                        {
                                                            salida = false;
                                                        }
                                                    }
                                                }
                                                else if(0<i&&(i<arr_ofertas.length)&&salida){
                                                    System.out.println("Oferta N°"+contador+"\nCorreo del comprador: "+arr_ofertas[i].split("-")[0]+"\nPrecio de Oferta: "+arr_ofertas[i].split("-")[1]);
                                                    contador++;
                                                    int decision = 0;
                                                    do{
                                                        decision =Sistema.numeroPorTeclado("\n1. Anterior Oferta\t\t2. Aceptar Oferta\t\t3.Siguiente Oferta\t\t\nSu respuesta » ");
                                                    }while(decision<1||decision>3);
                                                    switch(decision){
                                                        case 1:
                                                        {
                                                            i-=2;
                                                        }break;
                                                        case 2:
                                                        {
                                                            Venta.aceptarOferta(arr_ofertas[i].split("-")[0], tema, desarrollo, placa);
                                                            salida=false;
                                                        }break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                }
                            }break;
                        }
                        regresar = vendedor;
                    }while(regresar !=4);
                    respuesta = 0;
                } break;
                case 2:
                {
                    int regresar;
                    do{
//▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲COMPRADOR▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲
                        System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                        int comprador = 0;
                        do{
                            System.out.print("Opciones del comprador.\n1. Registrar un nuevo comprador\n2. Ofertar por un vehículo\n3. Regresar\nSu respuesta » ");
                            try{
                                Scanner sc = new Scanner(System.in);
                            comprador = sc.nextInt();
                            if(comprador<1||comprador>3){
                                System.out.println("\n*********************************************************************************************\n[#]No ingreso ningúna de las opciones mencionadas.");
                            }}catch(Exception e){
                                error = true;
                                System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número");
                            }
                        }while((comprador<1||comprador>3)&&(error));
                        switch (comprador) {
                            case 1:
                            {
                                System.out.println("->Registro\nNota: favor de ingresar correctamente datos únicos suyos y asegurarse que tiene cuenta de gmail para recibir información del programa,\nSaludos.");
                                us = Sistema.textoPorTeclado("Usuario: ");
                                email = Sistema.validarGmail();
                                if(Sistema.existeUsuario(us,email,"comprador")==2||Sistema.numeroLineas("Usuarios.txt")==0){
                                    user = Sistema.infoPersona1();
                                    pw = Sistema.textoPorTeclado("Clave: ");
                                    asunto = "Creación de cuenta";
                                    cuerpo = "Se ha creado correctamente su cuenta de vendedor en .\nRecuerde que su usuario es: \""+us+"\" y contraseña es: \""+pw+"\".\nConsiderar que la misma contraseña";
                                    Sistema.enviarConGMail(email, asunto, cuerpo);
                                    rol = new Rol(us, pw, email,"comprador");
                                    user.añadirCuenta(rol);
                                    rol.guardarClave();
                                }
                                else if(Sistema.existeUsuario(us,email,"comprador")==3)
                                    Rol.modificarCuenta(us, "vendedor", "comprador");
                                else
                                    System.out.println("Ya existe un usuario así como comprador.\nPuede intentar con otro si gusta.");
                            }break;
                            case 2:
                            {
                                int intentos = 0;
                                System.out.println("\n------------------------------------------------------------------------------------------\nIngrese los datos de su cuenta comprador:");
                                do{
                                    us = Sistema.textoPorTeclado("Usuario: ");
                                    pw = Sistema.textoPorTeclado("Clave: ");
                                    if(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("comprador", us)))&&(intentos<3)){
                                        System.out.println("El usuario y/o contraseña no son los correctos. Intente nuevamente");
                                        ++intentos;
                                    }
                                }while(((Sistema.verificarCuenta(us, pw))||(Rol.revisarUsuario("comprador", us)))&&(intentos<3));
                                if(intentos == 3){
                                    System.out.println("Al parecer no existe esa cuenta, vuelva a intentarlo o cree una cuenta.");
                                }
                                else{
                                    System.out.println("Revise la lista de vehículos y prepare su oferta.\nPara esto indique los parámetros que quisiera considerar para buscar su\nvehículo ideal.");
                                    int contador =1;
                                    String tipo="";
                                    ArrayList<String> mostrar= new ArrayList<>();
                                    System.out.println("Escriba [Y] para los parámetros que desea considerar o [N] caso contrario.");
                                    String tip;
                                    do{
                                        tip = Sistema.textoPorTeclado("Tipo de vehiculo (Y/N)» ");
                                    }while(!tip.equals("Y")&&!tip.equals("N"));
                                    if(tip.equals("Y")){
                                        do{
                                            tipo = Sistema.textoPorTeclado("Indique que tipo de vehiculo le interesa observar. [Carro,Moto,Camion,Camioneta,Furgoneta,Bus]\nSu respuesta » ");
                                        }while(!tipo.equals("Moto")&&!tipo.equals("Carro")&&!tipo.equals("Camion")&&!tipo.equals("Camioneta")&&!tipo.equals("Bus")&&!tipo.equals("Furgoneta"));
                                    }
                                    String year;
                                    do{
                                        year = Sistema.textoPorTeclado("Rango de anualidad del vehículo (Y/N)» ");
                                    }while(!year.equals("Y")&&!year.equals("N"));
                                    int añoI=0, añoF=0;
                                    if(year.equals("Y")){
                                        añoI = Sistema.numeroPorTeclado("Año inicial: ");
                                        añoF = Sistema.numeroPorTeclado("Año final: ");
                                    }
                                    String km;
                                    do{
                                        km = Sistema.textoPorTeclado("Rango de distancia que ha recorrido el vehículo (Y/N)» ");
                                    }while(!km.equals("Y")&&!km.equals("N"));
                                    int recorridoI=0,recorridoF=0;
                                    if(km.equals("Y")){
                                        recorridoI=Sistema.numeroPorTeclado("Recorrido inicial: ");
                                        recorridoF=Sistema.numeroPorTeclado("Recorrido final: ");
                                    }
                                    String pr;
                                    do{
                                        pr = Sistema.textoPorTeclado("Rango de precios (Y/N)» ");
                                    }while(!pr.equals("Y")&&!pr.equals("N"));
                                    double precioI=0,precioF=0;
                                    if(pr.equals("Y")){
                                        precioI=Sistema.decimalPorTeclado("Precio inicial: ");
                                        precioF=Sistema.decimalPorTeclado("Precio final: ");
                                    }
                                    mostrar = Sistema.aplicarCriterios(Sistema.leerLineasArchivos("Vehiculos.txt"), tipo, añoI, añoF, recorridoI, recorridoF, precioI, precioF);
                                    ArrayList<Rol> roles = Rol.rolDeUsuario();
                                    
                                    String[] vhc = Sistema.listaToArreglo(mostrar);
                                    for(int i=0;i<vhc.length;i++){
                                        if(i==0){
                                                Sistema.mostrarVehiculo(i, roles, vhc, contador);
                                                int decision = 0;
                                                do{
                                                    decision =Sistema.numeroPorTeclado("\n1. Añadir Oferta\t\t2. Siguiente\t\t3.SALIR\nSu respuesta » ");
                                                }while(decision<1||decision>3);
                                                switch(decision){
                                                    case 1:
                                                    {
                                                        Rol userx = new Rol(vhc[i].split(";")[0]);
                                                        Vehiculo vec = new Vehiculo(vhc[i].split(";")[vhc[i].split(";").length-3],vhc[i].split(";")[vhc[i].split(";").length-8],vhc[i].split(";")[vhc[i].split(";").length-7],vhc[i].split(";")[vhc[i].split(";").length-6],Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-5]),Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-4]));
                                                        Rol use = roles.get(roles.indexOf(userx));
                                                        double rebaja = Sistema.decimalPorTeclado("\n--------------------------------------------------------------\nIngrese su oferta por favor: ");
                                                        offer = new Oferta(rebaja);
                                                        offer.añadirOferta(vec, use);
                                                    }break;
                                                    case 3:
                                                    {
                                                        i=vhc.length;
                                                    }break;
                                                }
                                        }
                                        else if(i==(vhc.length-1)){
                                            Sistema.mostrarVehiculo(i, roles, vhc, contador);
                                            System.out.println("\n1. Anterior\t\t2. Añadir Oferta\t\t3.SALIR");
                                            int decision = 0;
                                            do{
                                                decision =Sistema.numeroPorTeclado("Su respuesta » ");
                                            }while(decision<1||decision>3);
                                            switch(decision){
                                                case 1:
                                                {
                                                    i-=2;
                                                }break;
                                                case 2:
                                                {
                                                    Rol userx = new Rol(vhc[i].split(";")[0]);
                                                    Vehiculo vec = new Vehiculo(vhc[i].split(";")[vhc[i].split(";").length-3],vhc[i].split(";")[vhc[i].split(";").length-8],vhc[i].split(";")[vhc[i].split(";").length-7],vhc[i].split(";")[vhc[i].split(";").length-6],Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-5]),Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-4]));
                                                    Rol use = roles.get(roles.indexOf(userx));
                                                    double rebaja = Sistema.decimalPorTeclado("\n--------------------------------------------------------------\nIngrese su oferta por favor: ");
                                                    offer = new Oferta(rebaja);
                                                    offer.añadirOferta(vec, use);
                                                }break;
                                                case 3:
                                                {
                                                    i=vhc.length;
                                                }break;
                                            }
                                        }
                                            else{
                                                Sistema.mostrarVehiculo(i, roles, vhc, contador);
                                                System.out.println("\n1. Anterior\t\t2. Añadir Oferta\t\t3. Siguiente");
                                                int decision = 0;
                                                do{
                                                    decision =Sistema.numeroPorTeclado("Su respuesta » ");
                                                }while(decision<1||decision>3);
                                                switch(decision){
                                                    case 1:
                                                    {
                                                        i-=2;
                                                    }break;
                                                    case 2:
                                                    {
                                                        Rol userx = new Rol(vhc[i].split(";")[0]);
                                                        Vehiculo vec = new Vehiculo(vhc[i].split(";")[vhc[i].split(";").length-3],vhc[i].split(";")[vhc[i].split(";").length-8],vhc[i].split(";")[vhc[i].split(";").length-7],vhc[i].split(";")[vhc[i].split(";").length-6],Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-5]),Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-4]));
                                                        Rol use = roles.get(roles.indexOf(userx));
                                                        double rebaja = Sistema.decimalPorTeclado("\n--------------------------------------------------------------\nIngrese su oferta por favor: ");
                                                        offer = new Oferta(rebaja);
                                                        offer.añadirOferta(vec, use);
                                                    }break;
                                                }
                                            }
                                    }
                                }
                            }
                        }
                        regresar = comprador;
                    }while(regresar != 3);
                } break;
        }
            System.out.println("\n================================================================================================\n");
        }while(respuesta != 3);
        
    }
    
}