﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Sistema {
    
    private Sistema(){}
        
    public static void creacionArchivos() throws IOException{
        File arch1 = new File("Usuarios.txt");
        if(!arch1.exists())
            arch1.createNewFile();
        File arch2 = new File("Seguridad.txt");
        if(!arch2.exists())
            arch2.createNewFile();
        File arch3 = new File("Ofertas.txt");
        if(!arch3.exists())
            arch3.createNewFile();
        File arch4 = new File("Vehiculos.txt");
        if(!arch4.exists())
            arch4.createNewFile();
    }
    
    public static int numeroLineas(String nombre){
        int contador = 0;
        try(Scanner sc = new Scanner(new File(nombre))){
            while(sc.hasNextLine()){
                contador++;
                sc.nextLine();
            }
        }
        catch(Exception e){}
        return contador;
    }
    
    public static void enviarConGMail(String destinatario, String asunto, String cuerpo) throws MessagingException {
        System.out.println("Enviando el mensaje, espere...");
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", "xxmotorsGuayas@gmail.com");
        props.put("mail.smtp.clave", "Antha@Car2020");    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google
        Session session = Session.getDefaultInstance(props);
        prepareMessage(session, asunto, cuerpo, destinatario);
    }
    
    private static void prepareMessage(Session session,String asunto,String cuerpo,String destinatario){
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress("XxmotorsGuayas@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect("smtp.gmail.com","XxmotorsGuayas@gmail.com", "Antha@Car2020");
                transport.sendMessage(message, message.getAllRecipients());
            }}catch (MessagingException me) {}
    }
    
    protected static byte[] getSHA(String input) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    } 

    protected static String toHexString(byte[] hash){
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 32){  
            hexString.insert(0, '0');  
        }
        return hexString.toString();  
    }
    
    public static int existeUsuario(String usuario, String correo,String tipo_user){
        //Este método permite verificar si el usuario a crear es vendedor, comprador o ninguno. Validacion en el main y para el metodo que agrega los tipo de usuario
        ArrayList<Rol> roles = Rol.rolDeUsuario();
        int opcion =0;
        for(Rol e: roles){
            if((e.getUsuario().equals(usuario)||e.getCorreo().equals(correo))&&e.getTipo_usuario().contains("/"))
                opcion+=4;
            else if ((e.getUsuario().equals(usuario)||e.getCorreo().equals(correo))&&!e.getTipo_usuario().equals(tipo_user))
                opcion+=3;
            else if ((e.getUsuario().equals(usuario)||e.getCorreo().equals(correo))&&e.getTipo_usuario().equals(tipo_user))
                opcion+=4;
            }
        if(opcion==0)
            opcion+=2;
        return opcion;
    }
    
    public static ArrayList<String> leerLineasArchivos(String name){
       ArrayList<String> hash = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(name))){
            while(sc.hasNextLine()){
                String linea = sc.nextLine();
                hash.add(linea);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return hash;
    }
    
    public static boolean verificarCuenta(String usuario, String clave) throws NoSuchAlgorithmException{
        //Este método permite verificar la cuenta en el main
        File archivo = new File("Seguridad.txt");
        boolean verificacion=true;
        try{
            if(archivo.exists()){
                ArrayList<String> codClaves = leerLineasArchivos("Seguridad.txt");
                for(String e: codClaves){
                    if(Sistema.toHexString(Sistema.getSHA(usuario+clave)).equals(e))
                        verificacion = false;
                }
            }
        }catch(Exception e){
            verificacion = false;
        }return verificacion;
    }
    
    public static boolean verificarPlaca(String placa){
        ArrayList<String> listaV = leerLineasArchivos("Vehiculos.txt");
        boolean existencia = false;
        for(String e: listaV){
            if(e.split(";")[e.split(";").length-3].equals(placa)){
                existencia = true;
            }
        }
        return existencia;
    }
    
    public static Persona infoPersona1(){
        System.out.print("Nombres: ");
        Scanner nombres = new Scanner(System.in);
        String name = nombres.nextLine();
        System.out.print("Apellidos: ");
        Scanner apellidos = new Scanner(System.in);
        String last =apellidos.nextLine();
        System.out.print("Organizacion: ");
        Scanner organizacion = new Scanner(System.in);
        String org = organizacion.nextLine();
        System.out.print("Cedula: ");
        Scanner cedula = new Scanner(System.in);
        String ced=cedula.nextLine();
        Persona user = new Persona(name, last, org, ced);
        return user;
    }
    
    public static String validarGmail(){
        String email;
        do{
            System.out.print("Gmail: ");
            Scanner correo = new Scanner(System.in);
            email = correo.nextLine();
            if(!email.contains("@"))
                System.out.println("Asegurese de que el correo ingresado contenga \"@gmail.com\"");
        }while(!email.contains("@"));
        return email;
    }
    
    public static String textoPorTeclado(String frase){
        System.out.print(frase);
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine();
        return cadena;
    }
    public static String VerificarString(String frase){
        int rep;
        String linea="";
        do{
        Scanner sc= new Scanner(System.in);
        System.out.println(frase);
        rep=0;
        linea=sc.nextLine();
        
        for(int i=0;i<linea.length();i++){
            try{
                int num=Integer.parseInt(linea.substring(i));
                rep++;
            }
            catch(Exception e){
                
            }
        }
    }while(rep!=0);
      return  linea;
    }
    
    public static int numeroPorTeclado(String frase){
        int number=-1;
        boolean error;
        
        do{
            try{
                System.out.print(frase);
                Scanner sc = new Scanner(System.in);
                number = sc.nextInt();
                error=false;
                if(number<=0)
                    System.out.println("El valor ingresado debe ser mayor a 0");
            }catch(Exception e){
                error=true;
                System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número");
            }}while(error&&number<=0);
        return number;
    }
    
    public static double decimalPorTeclado(String frase){
        double number = 0;
        boolean error = true;
        System.out.println(frase);
        do{
            try{
                Scanner sc = new Scanner(System.in);
                sc.useDelimiter("\n").useLocale(Locale.US);
                number = sc.nextDouble();
                error=false;
                if(number<=0)
                    System.out.println("El valor ingresado debe ser mayor a 0.");
            }catch(Exception e){
                error=true;
                System.out.println("\n*********************************************************************************************\n[!] Lo que ingresó no fué un número con decimales");
            }}while(error&&number<=0);
        return number;}
    
    public static ArrayList<String> aplicarCriterios(ArrayList<String> vehiculos,String tipo_vehiculo,int anualI, int anualF, int recorridoI, int recorridoF, double precioI, double precioF){
        ArrayList<String> mostrar=new ArrayList<>();
        if(!tipo_vehiculo.equals("")||(recorridoI!=0&&recorridoF!=0)||(anualI!=0&&anualF!=0)||(precioI!=0&&precioF!=0)){
            if(!tipo_vehiculo.equals("")){
                mostrar = Venta.criterioTipoUsuario(vehiculos, tipo_vehiculo);
            }if(anualI!=0&&anualF!=0){
                mostrar = Venta.validarCriterioAnual(mostrar,vehiculos,anualI,anualF);
            }if(recorridoI!=0&&recorridoF!=0){
                mostrar = Venta.validarCriterioDistancia(mostrar,vehiculos, recorridoI, recorridoF);
            }if(precioI!=0&&precioF!=0){
                mostrar = Venta.validarCriterioPrecio(mostrar,vehiculos, precioI, precioF);
        }}
        else
            mostrar=vehiculos;
        return mostrar;
    }
    
    public static String[] listaToArreglo(ArrayList<String> lista){
        StringBuilder x = new StringBuilder();
        for(String e : lista){
            x.append(e).append(">");
        }
        String[] arr = x.toString().split(">");
        return arr;
    }
    
    public static int identificarTipoVehiculo(String tipo){
        int valor =0;
        if(tipo.equals("Moto"))
            valor=1;
        else if(tipo.equals("Carro"))
            valor=2;
        else if(tipo.equals("Camion"))
            valor=3;
        else if(tipo.equals("Camioneta"))
            valor=4;
        else if(tipo.equals("Furgoneta"))
            valor=5;
        else if(tipo.equals("Bus"))
            valor=6;
        return valor;
    }
    
    public static void mostrarVehiculo(int i,ArrayList<Rol> roles,String[] vhc,int contador){
        Rol userx = new Rol(vhc[i].split(";")[0]);
        Vehiculo vec = new Vehiculo(vhc[i].split(";")[vhc[i].split(";").length-3],vhc[i].split(";")[vhc[i].split(";").length-8],vhc[i].split(";")[vhc[i].split(";").length-7],vhc[i].split(";")[vhc[i].split(";").length-6],Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-5]),Integer.parseInt(vhc[i].split(";")[vhc[i].split(";").length-4]));
        switch(Sistema.identificarTipoVehiculo(vhc[i].split(";")[vhc[i].split(";").length-1])){     
            case 1:
            {  
                Moto moto = new Moto(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],vhc[i].split(";")[7]);
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+moto);
            }break;
            case 2:
            {
                Carro carro = new Carro(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],Boolean.parseBoolean(vhc[i].split(";")[7]));
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+carro);
            }break;
            case 3:
            {
                Camion camion=new Camion(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],Double.parseDouble(vhc[i].split(";")[7]),Boolean.parseBoolean(vhc[i].split(";")[8]),Integer.parseInt(vhc[i].split(";")[9]));
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+camion);
            }break;
            case 4:
            {
                Camioneta camioneta=new Camioneta(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],vhc[i].split(";")[7]);
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+camioneta);
            }break;
            case 5:
            { 
                Furgoneta furgoneta = new Furgoneta(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],Integer.parseInt(vhc[i].split(";")[7]));
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+furgoneta);
            }break;
            case 6:
            {
                Bus bus = new Bus(Integer.parseInt(vhc[i].split(";")[6]),Integer.parseInt(vhc[i].split(";")[5]),Integer.parseInt(vhc[i].split(";")[4]),vhc[i].split(";")[3],vhc[i].split(";")[1],vhc[i].split(";")[2],Integer.parseInt(vhc[i].split(";")[7]));
                String correo = roles.get(roles.indexOf(userx)).getCorreo();
                System.out.print("\n▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
                System.out.println("Vehículo N°"+contador+"\nCorreo del vendedor: "+correo+"\n"+vec+"\n"+bus);
            }break;
        } 
   }
    
}
