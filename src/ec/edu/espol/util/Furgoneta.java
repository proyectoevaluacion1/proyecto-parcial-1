/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Mac�as
 */
public class Furgoneta extends Descripcion{
    private int asientos;
    
    public Furgoneta(){}
    
    public Furgoneta(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,int asientos){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.asientos=asientos;
    }

    public int getAsientos() {
        return asientos;
    }

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }

    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.asientos+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getA�o()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    @Override
    public String toString(){
        return "Transmisi�n del veh�culo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nN�mero de espejos: "+this.espejo+"\t\t\tN�mero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nVelocidad promedio: "+this.asientos;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Furgoneta other = (Furgoneta)obj;
        return this.asientos==other.asientos;
    }
}
