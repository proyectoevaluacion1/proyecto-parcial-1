﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.util.*;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Macias
 */
public class Vehiculo {
    private String placa, marca, color, motor;
    private int ruedas, año;
    private Venta venta;
    private Descripcion descripcion;
    
    public Vehiculo(String placa, String marca, String color, String motor, int ruedas, int año){
        this.placa=placa;
        this.marca=marca;
        this.color=color;
        this.motor=motor;
        this.ruedas=ruedas;
        this.año=año;
    }
    
    public Vehiculo(String placa, int año){
        this.placa=placa;
        this.año=año;
    }
    
    public Vehiculo(String placa){
        this.placa=placa;
    }
    
    public String getPlaca() {
        return placa;
    }

    public String getMarca() {
        return marca;
    }

    public String getColor() {
        return color;
    }

    public String getMotor() {
        return motor;
    }

    public int getRuedas() {
        return ruedas;
    }

    public int getAño() {
        return año;
    }

    public Venta getVenta() {
        return venta;
    }

    public Descripcion getDescripcion() {
        return descripcion;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public void setRuedas(int ruedas) {
        this.ruedas = ruedas;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public void setDescripcion(Descripcion descripcion) {
        this.descripcion = descripcion;
    }
    
    public static void vehiculoVendido(String placa) throws IOException{
        ArrayList<String> v = Sistema.leerLineasArchivos("Vehiculos.txt");
        File archivo = new File("Vehiculos.txt");
        archivo.delete();
        archivo.createNewFile();
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            for(String e: v){
                if(!e.split(";")[e.split(";").length-3].equals(placa)){
                    linea.println(e);
                }   
            }
        }catch(Exception e){}
    }
    
    public static ArrayList<Vehiculo> listaVehiculos(){
        //Este método me sirve para validar en un metodo que puede validar en el main sea la placa, 
        ArrayList<Vehiculo> listaV = new ArrayList<>();
        try(Scanner sc = new Scanner(new File("Vehiculos.txt"))){
            while(sc.hasNextLine()){
                String[] palabras = sc.nextLine().split(";");
                Vehiculo e = new Vehiculo(palabras[palabras.length-3],palabras[palabras.length-8],palabras[palabras.length-7],palabras[palabras.length-6],Integer.parseInt(palabras[palabras.length-5]),Integer.parseInt(palabras[palabras.length-4]));
                listaV.add(e);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return listaV;
    }

    
    @Override
    public String toString(){
    
        return "Marca: "+this.marca+"\t\t\tColor: "+this.color+"\t\t\tTipo de motor: "+this.motor+"\nNúmero de ruedas: "+this.ruedas+"\t\t\tAño de adquisición del vehículo: "+this.año+"\t\t\t\tPlaca: "+this.placa;
    }
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Vehiculo other = (Vehiculo)obj;
        return this.placa.equals(other.placa);
    }
}
