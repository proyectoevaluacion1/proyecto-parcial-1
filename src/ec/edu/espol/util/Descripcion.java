﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public abstract class Descripcion {
    protected int recorrido, pasajeros, espejo;
    protected String tipo_frenos, transmision, modelo;
    protected Vehiculo vehiculo;
    
    public Descripcion(){}
    
    public Descripcion(int recorrido, int pasajeros, int espejo, String tipo_frenos, String transmision, String modelo){
        this.recorrido = recorrido;
        this.pasajeros= pasajeros;
        this.espejo=espejo;
        this.tipo_frenos=tipo_frenos;
        this.transmision=transmision;
        this.modelo=modelo;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public int getEspejo() {
        return espejo;
    }

    public String getTipo_frenos() {
        return tipo_frenos;
    }

    public String getTransmision() {
        return transmision;
    }

    public String getModelo() {
        return modelo;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public void setEspejo(int espejo) {
        this.espejo = espejo;
    }

    public void setTipo_frenos(String tipo_frenos) {
        this.tipo_frenos = tipo_frenos;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    public abstract void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta, String tipo);
    
    @Override
    public String toString(){
        return "Transmisión del vehículo: "+this.transmision+"\t\t\tModelo"+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nNúmero de espejos: "+this.espejo+"\t\t\tNúmero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Descripcion other = (Descripcion)obj;
        return this.recorrido==other.recorrido && this.transmision.equals(other.transmision);
    }    
    
}
