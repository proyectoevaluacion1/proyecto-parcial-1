*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.util.*;
import java.security.*;
/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Rol{
    private String usuario, clave,correo,tipo_usuario;
    private ArrayList<Venta> ventas;
    private ArrayList<Persona> personas;
    
    public Rol(String usuario , String clave, String correo,String tipo_usuario){
        this.usuario=usuario;
        this.clave=clave;
        this.correo=correo;
        this.tipo_usuario=tipo_usuario;
        ventas = new ArrayList<>();
        personas = new ArrayList<>();
    }
    
    public Rol(String usuario , String clave, String correo){
        this.usuario=usuario;
        this.clave=clave;
        this.correo=correo;
        personas = new ArrayList<>();
    }
    
    public Rol(String usuario){
        this.usuario=usuario;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTipo_usuario() {
        return tipo_usuario;
    }

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTipo_usuario(String tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }

    public void setTipo_usuario(String tipo_usuario,String tipo_user){
        this.tipo_usuario = tipo_usuario+"/"+tipo_user;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }
    
    public void setPersonas() {
        this.personas = Persona.infoPersona();
    }
    
    public static void modificarCuenta(String usuario, String tipo_user, String tipo_user2) throws IOException{
        ArrayList<String> personas = leerUsuarios();
        File archivo = new File("Usuarios.txt");
        archivo.delete(); archivo.createNewFile();
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            for(String persona: personas){
                if(persona.split(">")[1].contains(tipo_user)&&persona.split(">")[0].contains(usuario)){
                    String doub_users = tipo_user+"/"+tipo_user2;
                    linea.println(persona.split(">")[0]+">"+doub_users);
                }else
                    linea.println(persona);
            }linea.close();
        }catch(IOException e){}
    }
    
    public static boolean revisarUsuario(String tipo_usuario,String user){
        ArrayList<Rol> roles = rolDeUsuario();
        boolean existencia= true;
        for(Rol rol: roles){
            if(rol.getUsuario().equals(user)){
                if(rol.getTipo_usuario().contains("/")){
                    if(rol.getTipo_usuario().split("/")[0].equals(tipo_usuario)||rol.getTipo_usuario().split("/")[1].equals(tipo_usuario))
                        existencia = false;
                    }
                else
                    if(rol.getTipo_usuario().equals(tipo_usuario))
                        existencia=false;
            }}return existencia;
    }

    public static ArrayList<Rol> rolDeUsuario(){
        ArrayList<String> personas = leerUsuarios();
        ArrayList<Rol> listaR = new ArrayList<>();
        try{
            for(String usuario: personas){
                String[] palabras = usuario.split(";");
                Rol e = new Rol(palabras[6].split(">")[0],palabras[5],palabras[4],palabras[6].split(">")[1]);
                listaR.add(e);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return listaR;
    }

    protected static ArrayList<String> leerUsuarios(){
        //ESTE MÉTODO SOLO LEE EL ARCHIVO PERSONAS
        ArrayList<String> personas = new ArrayList<>();
        try(Scanner sc = new Scanner(new File("Usuarios.txt"))){
            while(sc.hasNextLine()){
                String linea = sc.nextLine();
                personas.add(linea);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return personas;
    }
    
    public void guardarClave(){
        //ESTE AGREGA EL HASHCODE AL ARCHIVO SEGURIDAD PARA LUEGO VERIFICAR LA CONTRASEÑA
        File archivo = new File("Seguridad.txt");
        PrintWriter line;
        try(FileWriter registro = new FileWriter(archivo, true)){
            line = new PrintWriter(registro);
            line.println(Sistema.toHexString(Sistema.getSHA(usuario+clave)));
            line.close();
        }catch(IOException | NoSuchAlgorithmException e){
            System.out.println(e.getMessage());
        }
    }
    protected String añadirRol(){
        StringBuilder r = new StringBuilder();
        r.append(this.getCorreo()).append(";");
        String[] arreglo = this.clave.split("|");
        for(int i=0; i<arreglo.length-1;i++){
            r.append(arreglo[i]).append("#");
        }
        r.append(arreglo[arreglo.length-1]).append(";").append(this.usuario).append(">").append(this.tipo_usuario);
        return r.toString();
    }
    
    
    @Override
    public String toString(){
        return "Usuario: "+this.usuario+"\t\t\tClave: "+this.clave+"\t\t\tGmail: "+this.correo+"\t\t\tRoles que cumple: "+this.tipo_usuario;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Rol other = (Rol)obj;
        return this.usuario.equals(other.usuario);
    }
    
}