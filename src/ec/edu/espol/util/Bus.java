/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Bus extends Descripcion{
    private int vidrios;
    
    public Bus(){}
    
    public Bus(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,int vidrios){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.vidrios=vidrios;
    }

    public int getVidrios() {
        return vidrios;
    }

    public void setVidrios(int vidrios) {
        this.vidrios = vidrios;
    }
    
    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.vidrios+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getAño()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    
    @Override
    public String toString(){
        return "Transmisión del vehículo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nNúmero de espejos: "+this.espejo+"\t\t\tNúmero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nNúmero de ventanas: "+this.vidrios;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Bus other = (Bus)obj;
        return this.vidrios==other.vidrios;
    }
    
}