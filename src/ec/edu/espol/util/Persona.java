/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;
import java.io.*;
import java.util.*;
/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Persona {
    private String nombres,apellidos,organizacion,cedula;
    private Rol rol;
    
    public Persona(){
    }
    
    public Persona(String nombres, String apellidos, String organizacion, String cedula){
        this.nombres= nombres;
        this.apellidos= apellidos;
        this.organizacion=organizacion;
        this.cedula=cedula;
    }
    
    
    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public Rol getRol() {
        return rol;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public static ArrayList<Persona> infoPersona(){
        ArrayList<String> personas = Rol.leerUsuarios();
        ArrayList<Persona> listaP = new ArrayList<>();
        try/*(Scanner sc = new Scanner(new File("Usuarios.txt")))*/{
            for(String usuario: personas){
                String[] palabras = usuario.split(";");
                Persona e = new Persona(palabras[0],palabras[1],palabras[3],palabras[2]);
                listaP.add(e);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return listaP;
    }
    
    public void añadirCuenta(Rol rol){
        //ESTE AÑADE LA INFORMACION DE LA INSTANCIA DE ROL AL ARCHIVO PERSONA,INCLUYENDO EL TIPO DE DATO SI ES VENDEDOR O USUARIO
        File archivo = new File("Usuarios.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(this.getNombres()+";"+this.getApellidos()+";"+this.getCedula()+";"+this.getOrganizacion()+";"+rol.añadirRol());
            linea.close();
        }catch(IOException e){}
    }
    
   @Override
   public String toString(){
       return "Nombres: "+this.getNombres()+"\t\t\tApellidos: "+this.getApellidos()+"\nCédula: "+this.getCedula()+"\t\t\tOrganización"+this.getOrganizacion();
   }
}