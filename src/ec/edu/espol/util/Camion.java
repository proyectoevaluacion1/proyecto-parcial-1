/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Mac�as
 */
public class Camion extends Descripcion{
    private double capacidad_carga;
    private boolean camarote;
    private int ejes;
    
    public Camion(){}
    
    public Camion(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,double carga){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.capacidad_carga=carga;
        this.camarote=camarote;
        this.ejes=ejes;
    }

    public double getCapacidad_carga() {
        return capacidad_carga;
    }

    public boolean isCamarote() {
        return camarote;
    }

    public int getEjes() {
        return ejes;
    }

    public void setCapacidad_carga(double capacidad_carga) {
        this.capacidad_carga = capacidad_carga;
    }

    public void setCamarote(boolean camarote) {
        this.camarote = camarote;
    }

    public void setEjes(int ejes) {
        this.ejes = ejes;
    }
    
    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.capacidad_carga+";"+this.camarote+";"+this.ejes+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getA�o()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    @Override
    public String toString(){
        return "Transmisi�n del veh�culo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nN�mero de espejos: "+this.espejo+"\t\t\tN�mero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nCapacidad promedio de carga: "+this.capacidad_carga+"\t\t\tTiene camarote: "+this.camarote+"\nNumero de ejer: "+this.ejes;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Camion other = (Camion)obj;
        return this.capacidad_carga==other.capacidad_carga;
    }
}