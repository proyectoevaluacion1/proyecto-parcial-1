/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Mac�as
 */
public class Camioneta extends Descripcion{
    private String mecanismo_ventana;
    
    
    public Camioneta(){}
    
    public Camioneta(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,String mecanismo){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.mecanismo_ventana=mecanismo;
    }

    public String getMecanismo_ventana() {
        return mecanismo_ventana;
    }

    public void setMecanismo_ventana(String mecanismo_ventana) {
        this.mecanismo_ventana = mecanismo_ventana;
    }
    
    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.mecanismo_ventana+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getA�o()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    @Override
    public String toString(){
        return "Transmisi�n del veh�culo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nN�mero de espejos: "+this.espejo+"\t\t\tN�mero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nMecanismo para el control de las ventanas: "+this.mecanismo_ventana;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Camioneta other = (Camioneta)obj;
        return this.mecanismo_ventana==other.mecanismo_ventana;
    }
}