﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Venta {
    private double precio;
    private ArrayList<Vehiculo> vehiculos;
    private ArrayList<Oferta> ofertas;
    private Rol rol;

public Venta(double precio){
        this.precio=precio;
    }
    public String getToPrecio() {
        StringBuilder price = new StringBuilder();
        price.append(this.precio);
        return price.toString();
    }
    
    public double getPrecio(){
        return precio;
    }
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public Rol getRol() {
        return rol;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public static void aceptarOferta(String gmail, String tema, String desarrollo, String placa) throws MessagingException, IOException{
        Sistema.enviarConGMail(gmail, tema, desarrollo);
        Vehiculo.vehiculoVendido(placa);
        Oferta.ofertaConfirmada(placa);
        System.out.println("¡Correo enviado y Felicitacions por su venta!");
    }
    
    public static ArrayList<String> mostrarOfertas(String placa, ArrayList<String> ofertas){
        ArrayList<String> show = new ArrayList<>();
        StringBuilder s = new StringBuilder();
        for(String vh : ofertas){
            String placax = vh.split(">")[0];
            String[] xs = vh.split(">")[1].split(";");
            if(placax.equals(placa)){
                for(int i=0;i<xs.length;i++){
                s.append("Oferta ").append(i+1).append("\nCorreo: ").append(xs[i].split("-")[0]).append("\nPrecio Ofertado: ").append(xs[i].split("-")[1]);
                show.add(s.toString());
                }}}
        return show;
    }
    
    protected static ArrayList<String> criterioTipoUsuario(ArrayList<String> vehiculos,String tipo){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            String tipo_user = (v.split(";")[v.split(";").length-1]);
            if(tipo_user.equals(tipo)){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioPrecio(ArrayList<String> vehiculos,double precioInicial, double precioFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int precio = Integer.parseInt(v.split(";")[v.split(";").length-2]);
            if(precio>precioInicial&&precio<precioFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioAnual(ArrayList<String> vehiculos,int añoInicial, int añoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int año = Integer.parseInt(v.split(";")[v.split(";").length-4]);
            if(año>añoInicial&&año<añoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioDistancia(ArrayList<String> vehiculos,int recorridoInicial, int recorridoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int recorrido = Integer.parseInt(v.split(";")[1]);
            if(recorrido>recorridoInicial&&recorrido<recorridoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> validarCriterioAnual(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioAnual(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioAnual(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioDistancia(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioDistancia(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioDistancia(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioPrecio(ArrayList<String> mostrar,ArrayList<String> vehiculos,double valorI, double valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioPrecio(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioPrecio(mostrar, valorI, valorF);
        return lista;
    }

    protected static int contarLista(ArrayList<String> mostrar){
        int contador = 0;
        for(String x : mostrar){
            ++contador;
        }
        return contador;
    }
    
    @Override
    public String toString(){
        return "Precio: "+this.precio;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Venta other = (Venta)obj;
        return this.precio==other.precio;
    }
}




   /* public Venta(double precio,Vehiculo vehiculo){
        this.precio=precio;
    }
    public String getToPrecio() {
        StringBuilder price = new StringBuilder();
        price.append(this.precio);
        return price.toString();
    }
    
    public double getPrecio(){
        return precio;
    }
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public Rol getRol() {
        return rol;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
     static ArrayList<String> mostrarOfertas(String placa, ArrayList<String> ofertas){
        ArrayList<String> show = new ArrayList<>();
        StringBuilder s = new StringBuilder();
        for(String vh : ofertas){
            String placax = vh.split(">")[0];
            String[] xs = vh.split(">")[1].split(";");
            if(placax.equals(placa)){
                for(int i=0;i<xs.length;i++){
                s.append("Oferta ").append(i+1).append("\nCorreo: ").append(xs[i].split("-")[0]).append("\nPrecio Ofertado: ").append(xs[i].split("-")[1]);
                show.add(s.toString());
                }}}
        return show;
    }
    
    protected static ArrayList<String> criterioTipoUsuario(ArrayList<String> vehiculos,String tipo){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            String tipo_user = (v.split(";")[v.split(";").length-1]);
            if(tipo_user.equals(tipo)){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioPrecio(ArrayList<String> vehiculos,double precioInicial, double precioFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int precio = Integer.parseInt(v.split(";")[v.split(";").length-2]);
            if(precio>precioInicial&&precio<precioFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioAnual(ArrayList<String> vehiculos,int añoInicial, int añoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int año = Integer.parseInt(v.split(";")[v.split(";").length-4]);
            if(año>añoInicial&&año<añoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioDistancia(ArrayList<String> vehiculos,int recorridoInicial, int recorridoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int recorrido = Integer.parseInt(v.split(";")[1]);
            if(recorrido>recorridoInicial&&recorrido<recorridoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> validarCriterioAnual(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioAnual(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioAnual(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioDistancia(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioDistancia(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioDistancia(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioPrecio(ArrayList<String> mostrar,ArrayList<String> vehiculos,double valorI, double valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioPrecio(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioPrecio(mostrar, valorI, valorF);
        return lista;
    }

    protected static int contarLista(ArrayList<String> mostrar){
        int contador = 0;
        for(String x : mostrar){
            ++contador;
        }
        return contador;
    }
    public void AceptarOferta(ArrayList<String> ofertas,String placa,double rebaja){
        for(int i=0;i<ofertas.size();i++){
            String plac=ofertas.get(i).split("<")[0];
            String correo=ofertas.get(i).split("<")[1];
            double reb=Double.parseDouble(ofertas.get(i).split("-")[1]);
            if(placa.equals(plac) && rebaja==reb){
                ///eliminar oferta
            }
        }
        
    }
     public static void mostrarVehiculo(Vehiculo vehiculo ){
         //Hacemos dowcasting para cada tipo de vehiculos e imprimir sus datos
       if(vehiculo instanceof Moto){
           Moto moto=(Moto) vehiculo;
           System.out.println("******Moto*****");
           moto.toString();
           
       }
       else if(vehiculo instanceof Furgoneta){
           System.out.println("*******Furgoneta*******");
           Furgoneta fr=(Furgoneta)vehiculo;
           fr.toString();
       }
       else if(vehiculo instanceof Camioneta){
           System.out.println("*******Camioneta*******");
           Camioneta cm=(Camioneta) vehiculo;
           cm.toString();
       }
       else if(vehiculo instanceof Carro){
           System.out.println("*******CARRO******");
           Carro cr=(Carro)vehiculo;
           cr.toString();
       }
       else if(vehiculo instanceof Bus){
           System.out.println("******Bus******");
           Bus bs=(Bus)vehiculo;
           bs.toString();
       }
       else if(vehiculo instanceof Electrico){
           System.out.println("*****Electrico*****");
           Electrico el=(Electrico)vehiculo;
           el.toString();
       }
       else{
           System.out.println("No es un vehiculo");
       }
    }
     public static ArrayList<String> RangoDeElementos(int num){
        ArrayList<String> lista= new ArrayList();
        for(int i=1;i<=num;i++){
            lista.add(i+"");
        }
        return lista;
    }
     public String mostrarVehiculos(ArrayList<Vehiculo> vehiculos){
         String opcion="";
         ArrayList<String> rango=RangoDeElementos(vehiculos.size());
         do{
         Scanner sc= new Scanner(System.in);
         for(int i=0;i<vehiculos.size();i++){
             System.out.print(i+1+". ");mostrarVehiculo(vehiculos.get(i));
         }
         opcion=sc.nextLine();
         }while(!rango.contains(opcion));
         return opcion;
     }
     
     //para tipo de vehiculo
     public void BuscarVehiculo(String tipo,ArrayList<Vehiculo> vehiculos){
        String linea=tipo.toLowerCase();
         if(linea.equals("moto")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Moto){
                     Moto mt=(Moto) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         else if(linea.equals("carro")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Carro){
                     Carro mt=(Carro) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         else if(linea.equals("furgoneta")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Furgoneta){
                     Furgoneta mt=(Furgoneta) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         else if(linea.equals("camion")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Camion){
                     Camion mt=(Camion) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         else if(linea.equals("electrico")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Electrico){
                     Electrico mt=(Electrico) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         else if(linea.equals("camioneta")){
             for(int i=0;i<vehiculos.size();i++){
                 if(vehiculos.get(i) instanceof Camioneta){
                     Camioneta mt=(Camioneta) vehiculos.get(i);
                     mt.toString();
                 }
             }
         }
         
             
         
     }
     //para el anio
     public void BuscarVehiculo(int aniosuperior,int anioinferior,ArrayList<Vehiculo> vehiculos){
         for(int i=0;i<vehiculos.size();i++){
           int rango1=vehiculos.get(i).getAño();
           if(rango1>=anioinferior && rango1<=aniosuperior){
               mostrarVehiculo(vehiculos.get(i));
           }
         }
     }
     public void BuscarVehiculo(ArrayList<Vehiculo> vehiculos,int recorridosuperior,int recorridoinferior){
         for(int i=0;i<vehiculos.size();i++){
          Descripcion vh=(Descripcion) vehiculos.get(i);
           int rango1=vh.getRecorrido();
           if(rango1>=recorridoinferior && rango1<=recorridosuperior){
               mostrarVehiculo(vehiculos.get(i));
           }
         }
     }
     //para el precio
     public void BuscarVehiculo(double preciosuperior,double precioinferior,ArrayList<Venta> ventas){
         for(int i=0;i<ventas.size();i++){
             double costo=ventas.get(i).getPrecio();
             if(costo>=precioinferior && costo<=preciosuperior){
                 mostrarVehiculo(ventas.get(i).getVehiculo());
             }
             
         }
     }
     public void MiniMenu(ArrayList<Vehiculo> vehiculos){
         ArrayList<String> rango= RangoDeElementos(4);
         String opcion="";
         do{
         System.out.println("*******Ofertas vehiculos*********");
         System.out.println("1.Buscar vehiculo por tipo vehiculo\n"+"2.Buscar por recorrido\n"+"3.Buscar vehiculo por anio\n"+"4.Buscar vehiculo por precio\n");
         Scanner sc= new Scanner(System.in);
         opcion=sc.nextLine();
             
         }while(!rango.contains(opcion));
         if(opcion.equals("1")){
             System.out.println("Ingrese tipo del vehiculo: ");
             Scanner sc2= new Scanner(System.in);
             String tipo=sc2.nextLine();
             BuscarVehiculo(tipo,vehiculos);
         }
         else if(opcion.equals("2")){
             Scanner sc3=new Scanner(System.in);
             System.out.println("Ingrese recorrido inicial: ");
             int recorrI=sc3.nextInt();
             System.out.println("Ingrese recorrido finales: ");
             int recoorF=sc3.nextInt();
             BuscarVehiculo(vehiculos,recoorF,recorrI);
         }
         else if(opcion.equals("3")){
             Scanner sc4= new Scanner(System.in);
             System.out.println("Ingrese anio inicial: ");
             int anioI=sc4.nextInt();
             System.out.println("Ingrese anio final: ");
             int anioF=sc4.nextInt();
             BuscarVehiculo(anioF,anioI,vehiculos);
         }
         else if(opcion.equals("4")){
             Scanner sc5= new Scanner(System.in);
             System.out.println("Ingrese el precio inicial: ");
             int aniI=sc5.nextInt();
             System.out.println("Ingrese el precio final: ");
             int aniF=sc5.nextInt();
             BuscarVehiculo(aniF,aniI,vehiculos);
         }    
     }

    
    @Override
    public String toString(){
        return this.getToPrecio();
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Venta other = (Venta)obj;
        return this.precio==other.precio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
}*/
