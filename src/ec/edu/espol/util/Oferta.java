/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;
import static ec.edu.espol.util.Sistema.leerLineasArchivos;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.mail.MessagingException;
/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Oferta {
    private double rebaja;
    private Rol rol;
    private ArrayList<Venta> ventas;
    
    public Oferta(){}
    
    public Oferta(double rebaja){
        this.rebaja=rebaja;
    }

    public double getRebaja() {
        return rebaja;
    }

    public Rol getRol() {
        return rol;
    }

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public void setRebaja(double rebaja) {
        this.rebaja = rebaja;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }
    
    
    
    
    
    
    public void ofertar(Rol rol,Vehiculo vehiculo) throws MessagingException{
        String asunto = "Oferta de venta de un vehículo";
        String cuerpo = "Saludos,\n"+rol.getUsuario()+" le ofrece una oferta de: $"+this.rebaja+" para el vehiculo de placa: "+vehiculo.getPlaca();
        Sistema.enviarConGMail(rol.getCorreo(), asunto, cuerpo);
    }
    
    public void añadirOferta(Vehiculo v, Rol rol) throws IOException{
        File archivo = new File("Ofertas.txt");
        PrintWriter linea;
        if(!verificarPlacaOfertada(v.getPlaca())){
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(v.getPlaca()+">"+rol.getCorreo()+"-"+this.rebaja);
            linea.close();
        }catch(IOException e){}}
        else
            this.aumentarOfertas(v, rol);
    }
    
    private void aumentarOfertas(Vehiculo v, Rol rol) throws IOException{
        ArrayList<String> ofertas = agregar(v,rol,this.rebaja,Sistema.leerLineasArchivos("Ofertas.txt"));
        File archivo = new File("Ofertas.txt");
        archivo.delete(); archivo.createNewFile();
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            for(String of : ofertas){
                linea.println(of);
            }
        }
    }
    
    
    private static boolean verificarPlacaOfertada(String placa){
        ArrayList<String> listaV = leerLineasArchivos("Ofertas.txt");
        boolean existencia = false;
        for(String e: listaV){
            if(e.split(">")[0].equals(placa)){
                existencia = true;
            }
        }
        return existencia;
    }
    
    private static String repetirOfertas(String[] ofertas){
        StringBuilder r = new StringBuilder();
        for(int i=0; i<ofertas.length;i++){
            r.append(ofertas[i]);
            r.append(";");
        }
        return r.toString();
    }
    
    protected static String unirInfo(String correo, double rebaja){
        StringBuilder f = new StringBuilder();
        f.append(correo);
        f.append("-");
        f.append(rebaja);
        return f.toString();
    }
    
    protected static String unirOferta(String placa, String info){
        StringBuilder e = new StringBuilder();
        e.append(placa);
        e.append(">");
        e.append(info);
        return e.toString();
    }
    
    protected static String preparandoOferta(String placa, Vehiculo v, double rebaja, String[] ofertas, Rol rol){
        StringBuilder t = new StringBuilder();
        String y = repetirOfertas(ofertas);
        t.append(y);
        if(placa.equals(v.getPlaca())){
            String x = unirInfo(rol.getCorreo(),rebaja);
            t.append(x);
        }
        return t.toString();
    }
    
     public static void ofertaConfirmada(String placa) throws IOException{
        ArrayList<String> o = Sistema.leerLineasArchivos("Ofertas.txt");
        File archivo = new File("Ofertas.txt");
        archivo.delete();
        archivo.createNewFile();
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            for(String e: o){
                if(!e.split(">")[0].equals(placa)){
                    linea.println(e);
                }   
            }
        }catch(Exception e){}
    }
    
    protected static ArrayList<String> agregar(Vehiculo v, Rol rol,double rebaja,ArrayList<String> listaOfertas){
        ArrayList<String> offer = new ArrayList<>();
        for(String e: listaOfertas){
            StringBuilder s = new StringBuilder();
            String[] a = e.split(">");
            String placa = a[0];
            String[] ofertas = a[1].split(";");
            String cad_of=unirOferta(placa,preparandoOferta(placa,v,rebaja,ofertas,rol));
            offer.add(cad_of);
        }
        return offer;
    }
    
    @Override
    public String toString(){
        return ">Rebaja: "+this.rebaja;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Oferta other = (Oferta)obj;
        return this.rebaja==other.rebaja;
    }
}