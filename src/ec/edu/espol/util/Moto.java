﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Moto extends Descripcion{
    private String tipo_timon;
    
    public Moto(){}
    
    public Moto(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,String tipo_timon){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.tipo_timon=tipo_timon;
    }

    public String getTipo_timon() {
        return tipo_timon;
    }

    public void setTipo_timon(String tipo_timon) {
        this.tipo_timon = tipo_timon;
    }
    
    
    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.tipo_timon+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getAño()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    
    @Override
    public String toString(){
        return "Transmisión del vehículo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nNúmero de espejos: "+this.espejo+"\t\t\tNúmero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nTipo de timón: "+this.tipo_timon;
    }
    
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Moto other = (Moto)obj;
        return this.tipo_frenos.equals(other.tipo_frenos);
    }
    
}