/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Andres Medina
 * @author Freddy Tenesaca
 * @author Aaron Mac�as
 */
public class Carro extends Descripcion{
    private boolean descapotable;
    private int puertas;
    public Carro(){}
    
    public Carro(int recorrido, int pasajeros, int espejo, String tipo_frenos, String arranque, String modelo,boolean descapotable,int puertas){
        super(recorrido, pasajeros, espejo, tipo_frenos, arranque, modelo);
        this.descapotable=descapotable;
        this.puertas=puertas;
    }

    public boolean getDescapotable() {
        return descapotable;
    }

    public void setDescapotable(boolean descapotable) {
        this.descapotable = descapotable;
    }
    
    @Override
    public void agregarVehiculo(String usuario, Vehiculo vehic, Venta venta,String tipo){
        File archivo = new File("Vehiculos.txt");
        PrintWriter linea;
        try(FileWriter registro = new FileWriter(archivo, true)){
            linea = new PrintWriter(registro);
            linea.println(usuario+";"+this.transmision+";"+this.modelo+";"+this.tipo_frenos+";"+this.espejo+";"+this.pasajeros+";"+this.recorrido+";"+this.descapotable+";"+vehic.getMarca()+";"+vehic.getColor()+";"+vehic.getMotor()+";"+vehic.getRuedas()+";"+vehic.getA�o()+";"+vehic.getPlaca()+";"+venta.getPrecio()+";"+tipo);
            linea.close();
        }catch(IOException e){}
    }
    
    @Override
    public String toString(){
        return "Transmisi�n del veh�culo: "+this.transmision+"\t\t\tModelo: "+this.modelo+"\t\t\tSistema de frenado: "+this.tipo_frenos+"\nN�mero de espejos: "+this.espejo+"\t\t\tN�mero de pasajeros: "+this.pasajeros+"\t\t\tKilometraje del carro: "+this.recorrido+"\nEs descapotable: "+this.descapotable+"\t\t\tN�mero de puertas: "+this.puertas;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Carro other = (Carro)obj;
        return this.puertas==other.puertas;
    }
}